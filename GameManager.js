static var Modalidad:boolean=false;//false time , true score 
var Dificultad:int=10;
var cantidadBloques:int=0;
@HideInInspector
var cerradura:boolean=false;
var touch: Touch;
var SubirNivel: GameObject;
var Excelente:GameObject;
@HideInInspector
var contadorNiveles:int=0;
@HideInInspector
var contador:int=0;
@HideInInspector
var checkGameOver:boolean=false;
@HideInInspector
var checkWin:boolean=false;
var LevelName : String;
var Rows : float = 10;
var blocksPerRow : int = 10;
var DefaultblockSize : Vector2;
@HideInInspector
var blockSize : Vector2;

/*Tendrá que introducir la altura de la pantalla y el ancho que son compatibles
con el tamaño de bloque entrado, vamos a utilizar estas dos variables a escala
el tamaño de bloque con diferentes resoluciones.*/
var DefaultScreenHeight : int;
var DefaultScreenWidth : int;
@HideInInspector
var ScreenHeight : int;
@HideInInspector
var ScreenWidth : int;

//Filas:
var StartingRows : int; 
@HideInInspector
var UsedRows : int = 0;

/*Estos son los componentes que va a la eliminación en el juego, más
bloques que añadir, más difícil el juego es, asegúrese de que usted les da diferente
nombres y diferentes texturas.*/
class blockVars
{
    var Name : String;
    var texture : Texture2D;
}
var block : blockVars[]; //Esto mantendrá la información de los bloques.


/*Los poderes especiales, se añaden estos poderes al azar a los bloques y cuando usted machaca
un bloque que contiene uno de ellos, usted será capaz de utilizar este poder:*/

var SpecialPowerEnabled : boolean = false; 

class SpecialPowerVars
{
    var Name : String; //Must be unique
    var texture : Texture2D; //Must be uniqe
    //var Key = KeyCode.Alpha1; //Must be unique
    var Sound : AudioClip;
    @HideInInspector
    var Amount : int;
    var RemoveAll : boolean = false;
    var RemoveOneRow : boolean = false;
    var RemoveColor : boolean = false;
    @HideInInspector
    var ColorName : String;
    var RemoveRows : boolean = false;
    var AmountToRemove : int;
}
var SpecialPower : SpecialPowerVars[];

var SpecialPowerRange : int = 20; //Cuanto más alto = menos posibilidades de tener un bloque con un poder especial.
var SpecialPowerReload : float = 3; //El tiempo necesario para utilizar otro poder especial.
@HideInInspector
var SpecialPowerTimer : float = 0;

var SpecialPowerTexture : Texture2D; //Esta textura se dibujará sobre el bloque que tiene un poder especial.

var RowsToAddPowers : int; //El número de filas que necesita ser añadido para crear poderes especiales.


/*Esta clase tendrá la información que necesitamos para eliminar los bloques, lo hará
contener todos los bloques en la parte superior, inferior, izquierdo y derecho de todos los bloques + el nombre
posición y tamaño. Esto no se debe cambiar durante el juego.*/
class SlotVars
{
    var Name : String;
    var texture : Texture2D;
    var rect : Rect;
    var IsTaken : boolean = false;
    var Top : int;
    var Bottom : int;
    var Right : int;
    var Left : int;
    var Moving : boolean = false;
    var MovingRect : Rect;
    var MovingAmount : int;
    var SpecialPowerName : String = "None";
}
@HideInInspector
var Slot : SlotVars[];
@HideInInspector
var AreSlotsReady : boolean = false;
@HideInInspector
var AreblocksReady : boolean = false;
@HideInInspector
var Elimination : int[]; //Esto mantendrá los bloques para eliminar durante el juego.
@HideInInspector
var IsSearching : boolean = false;
@HideInInspector
var SearchOver : boolean = false;

//Game speed:
class SpeedVars
{
    var Default : float; //La velocidad del juego inicial.
    var Change : boolean = false; //Si desea acelerar el juego, marque esta opción.
    var Max : float; //La velocidad máxima que un jugador puede alcanzar.
    var ChangeEvery : float; /*Esto empezará a decrecer hasta el que llega a cero,
     cuando lo hace, se puede cambiar la velocidad.*/
    @HideInInspector
    var CountDown : float;
    @HideInInspector
    var CurrentValue : float;
    var ChangeValue : float; //El valor que se añadirá a la velocidad cada vez.
}
var Speed : SpeedVars;
@HideInInspector
var AddedRows : int; //¿Cuántas filas hemos añadido desde que comenzamos el juego.
@HideInInspector
var RowsToChangeSpeed : int; //Cuando esto es igual a Speed.ChangeEvery, la velocidad se cambia.



//Score:
class EliminationBonus
{
    var blocks : int;
    var Score : int;
}
var Bonus : EliminationBonus[];//Utilice esta opción si desea dar ventaja cuando un jugador elimina

class ScoreVars
{
    @HideInInspector
    var Value : int;
    var Minblocks : int = 3; //Número mínimo de bloques necesarios para eliminar para obtener puntuación.
    var ScorePerblock : int; /*Cuando se elimina un bloque, agregue esto a la variable score.
     un cierto número de bloques juntos.*/
    var PlayForScore : boolean = false; //El juego termina cuando un jugador alcanza una puntuación específica.
    var WinningScore : int; //La puntuación que el jugador necesita para llegar a ganar.
    var PlayForTime : boolean = false; //El juego termina cuando el jugador no alcanza una puntuación determinada en un tiempo limitado.
    var Minutes : int; //Limite el tiempo en minutos.
    //GUI relacionados Puntuación:
    @HideInInspector
    var JustAdded : int;
    @HideInInspector
    var LabelRect : Rect;
    @HideInInspector
    var LabelTimer : float; 
    @HideInInspector
    var HighScore : int;
}
var Score : ScoreVars;


//Textures:
var Emptyblock : Texture2D; //Esta textura se dibuja cuando el bloque está vacío.
var LimitLine : Texture2D;
var LosingLine : Texture2D;
var Background : Texture2D; /*Una imagen de fondo dibujado en toda la pantalla,
elegir uno que se adapte y se adapta al estilo de juego para que se vea bonito!*/


@HideInInspector
var IsRunning : boolean = false; //Si esto var es cierto, el juego está en marcha, si no, no se está ejecutando: D

//Sounds
var CrushSound : AudioClip;
var WinningSound : AudioClip;
var GameOverSound : AudioClip;
var NewRowSound : AudioClip;
var AlertSound : AudioClip;
var CountDownSound : AudioClip;
var NewRecordSound : AudioClip;

//Sound and music management:
var SoundON : Texture2D;
var SoundOFF : Texture2D;
@HideInInspector
var IsSoundON : boolean = true;

//Music:
var MusicON : Texture2D;
var MusicOFF : Texture2D;
@HideInInspector
var IsMusicON : boolean = true;
var Music : AudioClip;


//Time playing:
@HideInInspector
var Seconds : int = 0;
@HideInInspector
var Minutes : int = 0;


//GUI and showing the score:
var Style : GUIStyle;
//@HideInInspector
//var ColorTexto : Color; //= Vector4(0.0,23,255,255);

//Count down & Game over info:
@HideInInspector
var CountDown : float;
@HideInInspector
var GameOverTimer : float;
@HideInInspector
var GameOverMsg : String;

//----------------------multiresolucion--------------------------//
private var yE:float=10;
private var xP:float=570;
private var yP:float=570;
//----------------------------------------------------------------//

//----------------------------------------------------------------------------------------------------------------------
function Start(){
SubirNivel.SetActive(false);
Excelente.SetActive(false);
xP=global.ancho*(xP)/800;
yP=global.alto*(yP)/600;
yE=global.ancho*yE/800;
cerradura=false;
}

function Awake () 
{

//--------------------Inicio automatico-----------------------//////////
AreblocksReady = false;
CountDown = 3.5; //Start the count down timer.
if(CountDownSound) PlaySound(CountDownSound);
//-------------------------------------------------------------


    //Comprobación de los valores de las variables introducidas y ver si se ajustan a la secuencia de comandos, si no, depurar un error y desactivamos el guión.
    if(Rows < 10)
    {
        Debug.LogError("Game Manager Error: The minimum number of rows is 5!");
        enabled = false;
    }
    if(blocksPerRow < 5)
    {
        Debug.LogError("Game Manager Error: The minimum number of blocks per one row is 5!");
        enabled = false;
    }
    if(StartingRows > Rows)
    {
        Debug.LogError("Game Manager Error: Starting rows number can't be more than the rows number!");
        enabled = false;
    }
    if(DefaultblockSize.x <= 0 || DefaultblockSize.y <= 0)
    {
        Debug.LogError("Game Manager Error: block size (x or y) can not be equal to 0 or less than 0.");
        enabled = false;
    }
    if(DefaultScreenHeight <= 0 || DefaultScreenHeight <= 0)
    {
        Debug.LogError("Game Manager Error: Please set the default screen height and/or width in the inspector!");
        enabled = false;
    }
    if(StartingRows < 1)
    {
        Debug.LogError("Game Manager Error: The minimum number of starting rows is 1!");
        enabled = false;
    }
    if(block.Length < 2)
    {
        Debug.LogError("Game Manager Error: The minimum number of block types is 2!");
        enabled = false;
    }
    if(Speed.Default <= 0)
    {
        Debug.LogError("Game Manager Error: The game speed can't be equal to 0 or less than 0 (Check default speed)!");
        enabled = false;
    }
    if(Speed.Change == true && Speed.ChangeValue <= 0 || Speed.Change == true && Speed.Max <= 0)
    {
        Debug.LogError("Game Manager Error: The game speed can't be 0 or less than 0 (Check max speed/ speed change value)!");
        enabled = false;
    }
    
    //Establezca las filas usadas:
    UsedRows = StartingRows;
    //Set the screen height and width:
    ScreenHeight = DefaultScreenHeight;
    ScreenWidth = DefaultScreenWidth;
    //Set the block size height and width.
    blockSize = DefaultblockSize;
    //Ajuste el tamaño de las ranuras y establecer todas las ranuras rect:
    AreSlotsReady = false;
    AreblocksReady = false;
    Slot = new SlotVars[Rows * blocksPerRow];
    //Establecer el valor de la velocidad por defecto:
    Speed.CurrentValue = Speed.Default;
    Speed.CountDown += Speed.CurrentValue;
    AddedRows = 0;
    RowsToChangeSpeed = 0;
    
    IsRunning = false; //Game is not running.
    
    //Consigue la mayor puntuación alcanzada por el jugador:
    Score.HighScore = PlayerPrefs.GetInt("HighScore"+LevelName);
    
    
    //Cambiar la cantidad de poderes especiales:
    for(var i : int = 0; i < SpecialPower.Length; i++) //Iniciar un bucle en la matriz poderes especiales.
    {
        SpecialPower[i].Amount = 0;
    }
    
    Seconds = 0;
    Minutes = 0;
    PlaySound(Music);
    
    InvokeRepeating("SecondUpdate", 0, 1.0); //One second update.
    
}

function SecondUpdate ()
{
    //Cálculo del tiempo que el jugador pasó en este nivel de corriente. 
    if(IsRunning == true && AreblocksReady == true)
    {
        if(Seconds < 60)
        {
                Seconds++;
        }
        else
        {
            Minutes++;
            Seconds = 0;
        }
    }
}



function Update () 
{
    //Compruebe si la altura y / o anchura de la pantalla ha cambiado.
    if(ScreenHeight != Screen.height || ScreenWidth != Screen.width)
    {
        //Set the new screen height and width.
        ScreenHeight = Screen.height;
        ScreenWidth = Screen.width;
        /*Cambiar el tamaño de bloque (altura y anchura, así) y ejecute la prueba para ver
         si hay suficiente espacio para crear los bloques.*/
        ResolutionChange();
    }
    
    //Set the slot info:
    if(Slot.Length == Rows * blocksPerRow && AreSlotsReady == false) //Si las ranuras no están listos y tenemos el tamaño correcto.
    {
        SetSlotsRect();
    }
    
    var blockMoving = false;
    
    for(var i : int = 0; i < Slot.Length; i++) //Iniciar un bucle en las ranuras actuales:
    {
        if(Slot[i].IsTaken == true) //Compruebe si hay un bloque en esta ranura.
        {
            if(Slot[i].Moving == true) //Compruebe si el bloque en esta ranura se está moviendo.
            {
                blockMoving = true;
                if(Slot[i].MovingRect == Slot[i].rect)
                {
                    Slot[i].MovingRect = Slot[i].rect;
                    Slot[i].Moving = false;
                }
                else
                {
                    if(Slot[i].MovingAmount < 0)
                    {
                        if(Slot[i].rect.y < Slot[i].MovingRect.y + Slot[i].MovingAmount)
                        {
                            Slot[i].MovingRect.y += Slot[i].MovingAmount;
                        }
                        else
                        {
                            Slot[i].MovingRect.y = Slot[i].rect.y;
                            Slot[i].Moving = false;
                        }
                    }
                    else if(Slot[i].MovingAmount > 0)
                    {
                        if(Slot[i].rect.y > Slot[i].MovingRect.y + Slot[i].MovingAmount)
                        {
                            Slot[i].MovingRect.y += Slot[i].MovingAmount;
                        }
                        else
                        {
                            Slot[i].MovingRect.y = Slot[i].rect.y;
                            Slot[i].Moving = false;
                        }
                    }
                }
            }
            else if(i < (Rows * blocksPerRow) - blocksPerRow && IsSearching == false) //Compruebe si la ranura actual no está en la primera fila.
            {
                var NextSlot : int = i + blocksPerRow;
                var EndSearch : boolean = false;
                for(var s : int = 0; s < Rows; s++)
                {
                    if(NextSlot + blocksPerRow < Rows * blocksPerRow && Slot[NextSlot+blocksPerRow].IsTaken == false && EndSearch == false) 
                    {
                        NextSlot += blocksPerRow;
                    }
                    else
                    {
                        EndSearch = true;
                    }
                }
                if(Slot[i + blocksPerRow].IsTaken == false) //Verifique si no se toma el derecho ranura en la parte inferior de éste.
                {
                    //Move the block down:
                    Slot[NextSlot].IsTaken = true;
                    Slot[NextSlot].Name = Slot[i].Name;
                    Slot[NextSlot].texture = Slot[i].texture;
                    Slot[NextSlot].Moving = true;
                    Slot[NextSlot].MovingRect = Slot[i].rect;
                    Slot[NextSlot].MovingAmount = blockSize.y;
                    Slot[NextSlot].SpecialPowerName = Slot[i].SpecialPowerName;
                    //Restablecer la información de ranura actual.
                    Slot[i].IsTaken = false;
                    Slot[i].Name = "None";
                    Slot[i].texture = null;
                    Slot[i].Moving = false;
                    Slot[i].Moving = false;
                    Slot[i].SpecialPowerName = "None";
                }
            }
        }
    }
    
    //Mover primeros bloques de filas a la izquierda y la derecha:
    for(var t : int = 0; t < Slot.Length; t++) //Iniciar un bucle en las ranuras actuales:
    {
        if(t > (blocksPerRow * Rows) - blocksPerRow && Slot[t].Moving == false) //Compruebe que las ranuras actuales pertenecen a las primeras filas.
        {
            if(t >= (blocksPerRow * Rows) - blocksPerRow/2 && t != (blocksPerRow * Rows) - 1) //Si esta ranura se coloca a la derecha de la primera fila y no la última ranura.
            {
                if(Slot[t].IsTaken == false && Slot[t + 1].IsTaken == true && Slot[t + 1].Moving == false) //Si no es tomada por un bloque.
                {
                    var NextRight : int = t + blocksPerRow + 1;
                    var SearchRight : boolean = false;
                    for(var r : int = 0; r < Rows; r++)
                    {
                        if(Slot[NextRight-blocksPerRow].IsTaken == true && SearchRight == false && NextRight-blocksPerRow >= blocksPerRow) 
                        {
                             NextRight -= blocksPerRow;

                             Slot[NextRight - 1].IsTaken = true;
                             Slot[NextRight - 1].MovingAmount = blockSize.x;
                             Slot[NextRight - 1].Name = Slot[NextRight].Name;
                             Slot[NextRight - 1].texture = Slot[NextRight].texture;
                             Slot[NextRight - 1].SpecialPowerName = Slot[NextRight].SpecialPowerName;
                             Slot[NextRight - 1].MovingRect.x = Slot[NextRight].rect.x;
                             Slot[NextRight - 1].Moving = true;
                     
                             Slot[NextRight].IsTaken = false;
                             Slot[NextRight].Name = "None";
                             Slot[NextRight].SpecialPowerName = "None";
                        }
                        else
                        {
                            SearchRight = true;
                        }
                    }
                }
            }
        }
    }
    
    for(var t2 : int = (blocksPerRow * Rows) - 1; t2 > (blocksPerRow * Rows) - blocksPerRow; t2--) // Inicia un bucle en las ranuras actuales:
    {
        if(t2 > (blocksPerRow * Rows) - blocksPerRow && Slot[t2].Moving == false) //Compruebe que las ranuras actuales pertenecen a las primeras filas.
        {
            if(t2 < (blocksPerRow * Rows) - blocksPerRow/2 && t2 > (blocksPerRow * Rows) - (blocksPerRow)) //Si esta ranura se coloca a la izquierda de la primera fila.
            {
                if(Slot[t2].IsTaken == false && Slot[t2 - 1].IsTaken == true && Slot[t2 - 1].Moving == false) //Si no es tomada por un bloque.
                {
                    var NextLeft : int = t2 + blocksPerRow - 1;
                    var SearchLeft : boolean = false;
                    for(var l : int = 0; l < Rows; l++)
                    {
                        if(Slot[NextLeft-blocksPerRow].IsTaken == true && SearchLeft == false && NextLeft-blocksPerRow >= blocksPerRow) 
                        {
                             NextLeft -= blocksPerRow;
                             
                             Slot[NextLeft + 1].IsTaken = true;
                             Slot[NextLeft + 1].MovingAmount = blockSize.x;
                             Slot[NextLeft + 1].Name = Slot[NextLeft].Name;
                             Slot[NextLeft + 1].texture = Slot[NextLeft].texture;
                             Slot[NextLeft + 1].SpecialPowerName = Slot[NextLeft].SpecialPowerName;
                             Slot[NextLeft + 1].MovingRect.x = Slot[NextLeft].rect.x;
                             Slot[NextLeft + 1].Moving = true;
                     
                             Slot[NextLeft].IsTaken = false;
                             Slot[NextLeft].Name = "None";
                             Slot[NextLeft].SpecialPowerName = "None";
                        }
                        else
                        {
                            SearchLeft = true;
                        }
                    }
                }
            }
        }
    }
    
    /*Aquí, nosotros nos encargamos de añadir filas con la velocidad específica del juego.
     En primer lugar, tenemos que lidiar con la cuenta atrás, si es superior a 0, mantenga disminuyéndola
     hasta las que llega a cero, cuando lo hace, agregar una nueva fila.*/
    if(Speed.CountDown > 0 && IsRunning == true)
    {
        Speed.CountDown -= Time.deltaTime;
    }
    if(Speed.CountDown <= 0) //Adición de una fila:
    {
        Speed.CountDown = 0; //Restablecer la cuenta atrás de velocidad en primer lugar.
        if(Speed.Change == true) //Compruebe si vamos a cambiar la velocidad durante el juego.
        {
            if(Speed.Max <= Speed.CurrentValue) //Si no hemos alcanzado la velocidad máxima: 
            {
                if(RowsToChangeSpeed == Speed.ChangeEvery) //Si añadimos suficientes filas para cambiar la velocidad.
                {
                    Speed.CurrentValue -= Speed.ChangeValue; //reducir la velocidad actual.
                    RowsToChangeSpeed = 0; //Restablecer contando filas
                    if(Speed.CurrentValue < Speed.Max) //Si vamos a llegar a la velocidad máxima.
                    {
                        Speed.CurrentValue = Speed.Max;
                    }
                }
                //Iniciar una nueva cuenta atrás con un "nuevo" velocidad.
                Speed.CountDown += Speed.CurrentValue;
                RowsToChangeSpeed++;
                //Añadir una nueva fila.
                AddedRows++;
                AddRow();
            }
        }
        else
        {
            //Iniciar una nueva cuenta atrás con la misma velocidad.
            RowsToChangeSpeed = 0;
            Speed.CountDown += Speed.CurrentValue;
            //Añadir una nueva fila.
            AddedRows++;
            AddRow();
        }
    }
    
    
    //ganadora:
    if(Score.PlayForTime == true) //Si estamos jugando por un tiempo limitado!
        {
            if(Minutes >= Score.Minutes) //Si el tiempo ha terminado.
            {
           		Score.Minutes+=1;
           		contadorNiveles++;
          		 if(cantidadBloques<5)
           		{
	           		if(cerradura==true && contadorNiveles>=Dificultad) 
	           		{
	           		Score.Minblocks-=1;
	           		cantidadBloques++;
	           		cerradura=false;
	           		contadorNiveles=0;
	           		}
           		}
           		if(contadorNiveles>=Dificultad && cerradura==false)
           		{
           		Score.Minblocks+=1;
           		contadorNiveles=0;
           		cerradura=true;
           		} 
           		if(WinningSound) PlaySound(WinningSound);
           		SubirNivel.SetActive(true);
                //StopGame();
            }
        }
    if(Score.PlayForScore == true) //Si estamos jugando por unos puntos de calificación específicos
    {
        if(Score.Value >= Score.WinningScore) //Si llegamos a la cantidad de puntos de calificación para ganar:
        {
           Score.WinningScore+=100;
           contadorNiveles++;
           if(cantidadBloques<5)
           	{
	           if(cerradura==true && contadorNiveles>=Dificultad) 
	           	{
	           	Score.Minblocks-=1;
	           	cantidadBloques++;
	           	cerradura=false;
	           	contadorNiveles=0;
	           	}
           	}
           if(contadorNiveles>=Dificultad && cerradura==false)
           	{
           	Score.Minblocks+=1;
           	contadorNiveles=0;
           	cerradura=true;
           	} 
           if(WinningSound) PlaySound(WinningSound);
           SubirNivel.SetActive(true);
           //StopGame();
        }
    }
    
    
    //Listado de los puntos de calificación:
    if(Score.LabelTimer > 0)
    {
        Score.LabelTimer -= Time.deltaTime;
    }
    if(Score.LabelTimer < 0)
    {
        Score.LabelTimer = 0;
    }    
    
    
    //3 segundos de cuenta atrás antes de que el juego comienza aquí va:
    if(CountDown > 0)
    {
        CountDown -= Time.deltaTime;
    }
    if(CountDown < 0)
    {
        CountDown = 0;
        IsRunning = true;
    }
    
    //Si el juego no se está ejecutando, no crear bloques:
    if(IsRunning == false && CountDown == 0)
    {
        AreblocksReady = true;
    }
    
    
    //Game over mensaje de cuenta atrás.
    if(checkGameOver==true/*GameOverTimer > 0*/) //Código de cuenta regresiva:
    {
    print(checkGameOver);
    print(contador);
    if(contador<250)
    	{
    	contador++;
    	}
    if(contador==250)
		    	{
		    	/*if(Input.touchCount>0 || Input.GetMouseButton(0))
			        		{*/
			        		Musica.sonido=0;
			        		contador=0;
			        		checkGameOver=false;
			        		Application.LoadLevel("Publicidad");
			        		//}
		    	}
    //GameOverTimer -= Time.deltaTime;
    }
    /*if(GameOverTimer < 0) //Si el tiempo se ha terminado, y luego se detiene mostrando el nuevo mensaje.
    {
    GameOverTimer = 0;
    }*/
    
    
    //Poderes especiales:
    if(SpecialPower.Length != null) //Si tenemos poderes especiales creadas.
    {
        for(var x : int = 0; x < SpecialPower.Length; x++) //Iniciar un bucle en los poderes especiales:
        {
            //Si pulsamos la tecla que permite a este poder especial y si tenemos suficiente cantidad para usarlo.
            if(/*Input.GetKey(SpecialPower[x].Key) &&*/ SpecialPower[x].Amount >= 1 && SpecialPowerTimer == 0 && SpecialPowerEnabled == true)
            {
                SpecialPower[x].Amount--; //Reducir la cantidad.
                if(SpecialPower[x].Sound) PlaySound(SpecialPower[x].Sound); //Reproduzca el sonido de un poder especial, si existe.
                SpecialPowerTimer = SpecialPowerReload; //Inicie el temporizador para utilizar otro poder especial.
                //Ahora, vamos a empezar a comprobar los poderes especiales y las aplicamos:
//--------------------------------------------------------------------------------------//
                if(SpecialPower[x].RemoveAll == true) //Retire todos los bloques.
                {
                    for(var f : int = 0; f < Slot.Length; f++) //Iniciar un bucle en las ranuras de juego:
                    {
                        if(Slot[f].IsTaken == true) //Si se toma la ranura.
                        {
                            //Restablecer toda la información ranuras.
                            Slot[f].IsTaken = false;
                            Slot[f].Name = "None";
                            Slot[f].Moving = false;
                            Score.Value += Score.ScorePerblock;
                            Score.Value += 10;
                            Excelente.SetActive(true);
                        }
                    }
                }
//--------------------------------------------------------------------------------------//                
                if(SpecialPower[x].RemoveOneRow == true) //Retire la primera fila:
                {
                    for(var y : int = 0; y < Slot.Length; y++) //Iniciar un bucle en las ranuras de juego:
                    {
                        if(Slot[y].IsTaken == true && y >= (blocksPerRow * Rows) - blocksPerRow) //Busque la primera fila
                        {
                            //Restablecer el primero info cuadras de fila.
                            Slot[y].IsTaken = false;
                            Slot[y].Name = "None";
                            Slot[y].Moving = false;
                            Score.Value += Score.ScorePerblock;
                        }
                    }
                }
//--------------------------------------------------------------------------------------//                
                if(SpecialPower[x].RemoveColor == true) //Retire todas las ranuras que tienen el mismo color.
                {
                    var RandomColor : int = Random.Range(0,block.Length);
                    SpecialPower[x].ColorName = block[RandomColor].Name;
                    for(var z : int = 0; z < Slot.Length; z++) //Iniciar un bucle en las ranuras de juego:
                    {
                        //En primer lugar, la búsqueda de color para eliminarlos, al azar!
                        if(Slot[z].IsTaken == true && Slot[z].Name == SpecialPower[x].ColorName) //Si se toma de ranura y el nombre coincide (color matches).
                        {
                            //Restablecer toda la información ranuras del mismo color.
                            Slot[z].IsTaken = false;
                            Slot[z].Name = "None";
                            Slot[z].Moving = false;
                            Score.Value += Score.ScorePerblock;
                        }
                    }
                } 
//--------------------------------------------------------------------------------------//               
                if(SpecialPower[x].RemoveRows == true) //Eliminar las filas en función del número
                {
                    for(var w : int = 0; w < Slot.Length; w++) //Iniciar un bucle en las ranuras de juego:
                    {
                        if(Slot[w].IsTaken == true && w >= (blocksPerRow * Rows) - (blocksPerRow * SpecialPower[x].AmountToRemove)) //Search for the rows to remove.
                        {
                            //Restablecer los bloques que se colocan en la información de filas dado.
                            Slot[w].IsTaken = false;
                            Slot[w].Name = "None";
                            Slot[w].Moving = false;
                            Score.Value += Score.ScorePerblock;
                        }
                    }
                }  
            }
        }
    }
//--------------------------------------------------------------------------------------//    
    //Especial el tiempo de recarga de energía:
    if(SpecialPowerTimer > 0)
    {
        SpecialPowerTimer -= Time.deltaTime;
    }
    if(SpecialPowerTimer < 0)
    {
        SpecialPowerTimer = 0;
    }
}

function SetSlotsRect ()
{ 
    blockSize.x = (ScreenWidth * DefaultblockSize.x) / DefaultScreenWidth;
    blockSize.y = (ScreenHeight * DefaultblockSize.y) / DefaultScreenHeight;
    
    //Las posiciones de tragamonedas se crean a partir de estas dos variables.
    var SlotX = blockSize.x;
    var SlotY = ScreenHeight/2 - (blockSize.y * Rows/2); 
    
    
    var RowNumber : int = 1;
    var blockNumber : int = 1;
    
    
    for(var i : int = 0; i < Slot.Length; i++) //En primer lugar, creamos un bucle dentro de las ranuras actuales.
    { 
        Slot[i] = new SlotVars();
        //Establezca las variables de ranura actuales:
        Slot[i].IsTaken = false;
        Slot[i].Moving = false;
        Slot[i].Left = -1;
        Slot[i].Bottom = -1;
        Slot[i].Right = -1;
        Slot[i].Top = -1;
        Slot[i].Name = "None";
        
        
        Slot[i].rect = Rect(SlotX,SlotY,blockSize.x, blockSize.y);
        
        
        /*Ahora, vamos a establecer la parte inferior, izquierda, derecha y superior vars de esta ranura.
         ¿Qué son los bloques que se pueden colocar en esta dirección de la ranura.
         -1 Significa que no puede haber un bloque en esa dirección, -2 es el opuesto.*/
        if(RowNumber == 1) //Si esta es la primera fila:
        {
            Slot[i].Top = -2;
        }
        if(blockNumber == 1) //Si este es el primer bloque en la fila:
        {
            Slot[i].Left = -2;
        }
        if(blockNumber == blocksPerRow) //Si este es el último bloque en esta fila:
        {
            Slot[i].Right = -2;
        }
        if(RowNumber == Rows) //Si esta es la última fila:
        {
            Slot[i].Bottom = -2;
        }
        
        
        if(blockNumber < blocksPerRow) //Si todavía estamos en la misma fila.
        {
            SlotX += blockSize.x;
            //Siguiente posición del bloque:
            blockNumber++;
        }
        else if(blockNumber == blocksPerRow) //Si es el último bloque de la fila.
        {
            blockNumber = 1;
            RowNumber++;
            //Posición del bloque siguiente.
            SlotX = blockSize.x; 
            SlotY += blockSize.y;
        }
        
        
        if(Slot[i].Top == -1) //Si esto no es en la primera fila.
        {
            Slot[i].Top = i - blocksPerRow; //establecer la ranura superior.
        }
        if(Slot[i].Bottom == -1) //Si esto no es en la última fila.
        {
            Slot[i].Bottom = i + blocksPerRow; //establecer la ranura superior.
        }
        if(Slot[i].Left == -1) //Si este no es el primer bloque en una fila.
        {
            Slot[i].Left = i - 1;
        }
        if(Slot[i].Right == -1) //Si este no es el último bloque en una fila.
        {
            Slot[i].Right = i + 1;
        }
    }
    
    
    AreSlotsReady = true;
}


function UpdateSlotsRect()
{
    //Las posiciones de tragamonedas se crean a partir de estas dos variables.
    var SlotX = blockSize.x; 
    var SlotY = ScreenHeight/2 - (blockSize.y * Rows/2); 
    
    var RowNumber : int = 1;
    var blockNumber : int = 1;
    
    
    for(var i : int = 0; i < Slot.Length; i++) //En primer lugar, creamos un bucle dentro de las ranuras actuales.
    {
        Slot[i].rect = Rect(SlotX,SlotY,blockSize.x, blockSize.y);
        
        if(blockNumber < blocksPerRow) //Si todavía estamos en la misma fila.
        {
            SlotX += blockSize.x;
            //Siguiente posición del bloque:
            blockNumber++;
        }
        else if(blockNumber == blocksPerRow) //Si es el último bloque de la fila.
        {
            blockNumber = 1;
            RowNumber++;
            //Posición del bloque siguiente.
            SlotX = blockSize.x; 
            SlotY += blockSize.y;
        }
    }
}


function ResolutionChange ()
{
    /*Lo primero que debe hacer es ampliar el tamaño de bloque 
    (donde tenemos todas las posiciones de IU y tamaños de) con la resolución de pantalla actual.*/
    blockSize.x = (ScreenWidth * DefaultblockSize.x) / DefaultScreenWidth;
    blockSize.y = (ScreenHeight * DefaultblockSize.y) / DefaultScreenHeight;
    
    
    //Comprobamos siempre si podemos dibujar las filas y bloques usando la resolución y el tamaño de bloque actual:
    if((blockSize.x * blocksPerRow) + blockSize.x > ScreenWidth) //no hay espacio para los bloques en esta resolución actual:
    {
        Debug.LogError("Game Manager Error: There's no space to create blocks in this current resolution, please reduce the block size (x) or increase the current resolution (width)!");
        enabled = false;
    }
    if((blockSize.y * Rows) + blockSize.y > ScreenHeight) //no hay espacio para los bloques en esta resolución actual:
    {
        Debug.LogError("Game Manager Error: There's no space to create blocks in this current resolution, please reduce the block size (y) or increase the current resolution (height)!");
        enabled = false;
    }
    
    if(AreSlotsReady)
    {
        UpdateSlotsRect();
    }    
}

function OnGUI()
{
    if(AreSlotsReady == false) //Compruebe que las ranuras no están listos.
    {
        return;
    }
    
    
    //GUI.DrawTexture(new Rect(0,0,ScreenWidth, ScreenHeight),Background,ScaleMode.StretchToFill); //Draw the background texture.
       
    
    GUI.color.a = 0.8F;
    //Dibujo de los bloques vacíos texturas:
    for(var a : int = 0; a < Slot.Length; a++) //En primer lugar, iniciar un bucle en las ranuras actuales:
    {
        GUI.DrawTexture(Slot[a].rect,Emptyblock,ScaleMode.StretchToFill);
    }
    GUI.color.a = 1F;
    
    
    //Sound management:
    //Sound button:
    //var SoundRect = new Rect(ScreenWidth - blockSize.y*1.5,0,blockSize.y*1.5, blockSize.y*1.5);
    var SoundTexture : Texture2D;
    if(IsSoundON == true)
    {
        SoundTexture = SoundON;
    }
    else
    {
        SoundTexture = SoundOFF;
    }
    
   /* if(GUI.Button(SoundRect, SoundTexture))
    {
        IsSoundON = !IsSoundON;
    }*/
    
    
    //Music Button:
   // var MusicRect = new Rect(ScreenWidth - blockSize.y*3,0,blockSize.y*1.5, blockSize.y*1.5);
    var MusicTexture : Texture2D;
    if(IsMusicON == true)
    {
        MusicTexture = MusicON;
    }
    else
    {
        MusicTexture = MusicOFF;
    }
    
    /*if(GUI.Button(MusicRect, MusicTexture))
    {
        IsMusicON = !IsMusicON;
        if(IsMusicON == true)
        {
            PlaySound(Music);
        }
        else
        {
            audio.Stop();
        }
    }*/
    
    

    
    //Ahora vamos a empezar a añadir algunos bloques:
    if(AreblocksReady == false && AreSlotsReady == true) //Compruebe si no hemos creado los bloques todavía.
    {
        Createblocks();
    }
    else
    {
        for(var b : int = 0; b < Slot.Length; b++) //En primer lugar, iniciar un bucle en las ranuras actuales:
        {
            if(Slot[b].IsTaken == true) //Compruebe si se toma la ranura.
            {
                if(Slot[b].Moving == true) //Si la ranura actual se está moviendo.
                {
                    GUI.DrawTexture(Slot[b].MovingRect,Slot[b].texture,ScaleMode.StretchToFill);
                    for(var k : int = 0; k < SpecialPower.Length; k++) //Iniciar un bucle en la matriz poderes especiales.
                    {
                        if(Slot[b].SpecialPowerName == SpecialPower[k].Name) //Si la ranura actual tiene un poder especial
                        {
                            GUI.Label(Rect(Slot[b].MovingRect.x + blockSize.x/4, Slot[b].MovingRect.y + blockSize.y/4, blockSize.x - blockSize.x/2, blockSize.y - blockSize.y/2),SpecialPowerTexture, Style);
                        }
                    }
                }
                else
                {
                    GUI.DrawTexture(Slot[b].rect,Slot[b].texture,ScaleMode.StretchToFill); //Dibuje la textura franjas horarias.
                    for(var q : int = 0; q < SpecialPower.Length; q++) //Iniciar un bucle en la matriz poderes especiales.
                    {
                        if(Slot[b].SpecialPowerName == SpecialPower[q].Name) //Si la ranura actual tiene un poder especial
                        {
                            GUI.Label(Rect(Slot[b].rect.x + blockSize.x/4, Slot[b].rect.y + blockSize.y/4, blockSize.x - blockSize.x/2, blockSize.y - blockSize.y/2),SpecialPowerTexture, Style);
                        }
                    }
                }   
                if(Slot[b].rect.Contains(Event.current.mousePosition) && Slot[b].Moving == false && IsRunning == true) //Compruebe si el ratón del jugador es más de una manzana.
                {
                var TempArray = new Array(Elimination);
                    if(Input.GetMouseButtonDown(0) && IsSearching == false && Pausa.pausado==false) //Si los jugadores presiona el botón izquierdo del ratón:
                    {
                        //Agregar el bloque actual a la matriz de la eliminación.
                        //var TempArray = new Array(Elimination);
	                    TempArray.Add(b);
	                    Elimination = TempArray.ToBuiltin(int);
                        SearchForblocks(); 
                        IsSearching = true;
                    }
                    for (var i = 0; i < Input.touchCount; ++i) 
	        {   
		            if(Input.touchCount > 0 && IsSearching == false)
		            {
		            touch = Input.touches[0];  
		            var ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
					if(touch.phase == TouchPhase.Began && Pausa.pausado==false)
						{      
		               //Agregar el bloque actual a la matriz de la eliminación.
	                    TempArray.Add(b);
	                    Elimination = TempArray.ToBuiltin(int);
                        SearchForblocks(); 
                        IsSearching = true;
                        }                           	           
		            }
	        } 
                }
            }              
        }
    }

    //Los ajustes siguientes se aplican en la etiqueta que muestra la cantidad de puntaje se agregó.
    Style.fontSize = blockSize.y;//Puntajes cuando se explota un bloque
    //Style.fontSize = blockSize.y * 1.5;
    //ColorTexto= Vector4(45,200,240,255);//76,197,236,255
    //Style.normal.textColor = ColorTexto;
    
    //La cuenta atrás antes de que comience el partido:
    if(CountDown > 0)
    {
        GUI.Label(Rect(blockSize.x + (blockSize.x * blocksPerRow/2), ScreenHeight/2 - blockSize.y/2, blockSize.x/2, blockSize.y), Mathf.Round(CountDown).ToString(), Style);
    }    
    
    //Si el jugador hizo un nuevo récord:
    if(GameOverTimer > 0)
    {
        //GUI.Label(Rect(blockSize.x, ScreenHeight/2 - blockSize.y/2, blockSize.x * blocksPerRow, blockSize.y), GameOverMsg, Style);
        GUI.Label(Rect(blockSize.x + (blockSize.x * blocksPerRow/2), ScreenHeight/2 - blockSize.y/2, blockSize.x/2, blockSize.y), GameOverMsg, Style);
    }
    
    if(IsSearching == true && SearchOver == true)
    {
        if(Elimination.Length >= Score.Minblocks)
        {
            Score.JustAdded = 0;
            for(var c : int = 0; c < Elimination.Length; c++) //En primer lugar, iniciar un bucle en las ranuras actuales:
            {
                for(var s : int = 0; s < SpecialPower.Length; s++) //Iniciar un bucle en la matriz poderes especiales.
                {
                    if(Slot[Elimination[c]].SpecialPowerName == SpecialPower[s].Name) //Compruebe si el intervalo actual tiene un poder especial.
                    {
                        SpecialPower[s].Amount++; //Añádelo a los poderes especiales.
                    }
                }
                
                //Restablecer info del bloque eliminado:
                Slot[Elimination[c]].IsTaken = false;
                Slot[Elimination[c]].Name = "None";
                Slot[Elimination[c]].texture = null;
                Slot[Elimination[c]].Moving = false;
                
                //Aumentar la puntuación.
                Score.Value += Score.ScorePerblock;
                Score.JustAdded += Score.ScorePerblock;
            }
            
            var CurrentBonus : int = 0; //¿Cuánto bono vamos a añadir a la partitura.
            
            if(Bonus != null) //Si creamos bono de puntaje para el jugador.
            {
                for(var d : int = 0; d < Bonus.Length; d++) //Loop dentro de la gama de bonificación.
                {
                    if(Elimination.Length >= Bonus[d].blocks) //Compruebe si el número de bloques eliminados es igual o superior a la bonificación actual.
                    {
                        if(CurrentBonus < Bonus[d].Score)
                        {
                            CurrentBonus = Bonus[d].Score;
                        }
                    }
                }
            }    
            
            Score.Value += CurrentBonus;
            Score.JustAdded += CurrentBonus;
            Score.LabelTimer = 0.5;
            Score.LabelRect = Rect(Event.current.mousePosition.x + 10, Event.current.mousePosition.y - blockSize.y/2, blockSize.x * 1.5, blockSize.y * 2);
            if(CrushSound)
            {
                PlaySound(CrushSound);
            }
        }
        var TempArray2 = new Array(Elimination);
	    TempArray2.Clear();
	    Elimination = TempArray2.ToBuiltin(int);
        IsSearching = false;
    }
    
    //Dibuja las texturas límites alrededor de los bloques (abajo, izquierda y derecha).
    
    //Down:
    var LineX = 0; 
    var LineY = ScreenHeight/2 + (blockSize.y * Rows/2);
    GUI.DrawTexture(new Rect(LineX,LineY,blockSize.x * (blocksPerRow + 2), blockSize.y),LimitLine,ScaleMode.StretchToFill);
    
    //Left
    LineX = 0; 
    LineY = ScreenHeight/2 - (blockSize.y * Rows/2);
    GUI.DrawTexture(new Rect(LineX,LineY,blockSize.x, blockSize.y * Rows),LimitLine,ScaleMode.StretchToFill);
    
    //Right
    LineX = (blockSize.x * blocksPerRow); 
    LineY = ScreenHeight/2 - (blockSize.y * Rows/2);
    GUI.DrawTexture(new Rect(LineX + blockSize.x,LineY,blockSize.x, blockSize.y * Rows),LimitLine,ScaleMode.StretchToFill);
    
    
    
    //Dibuje la textura línea perdedora en la parte superior de los bloques.
    LineX = 0; 
    LineY = ScreenHeight/2 - (blockSize.y * Rows/2);
    GUI.DrawTexture(new Rect(LineX,LineY - blockSize.y,blockSize.x * (blocksPerRow + 2), blockSize.y),LosingLine,ScaleMode.StretchToFill);
    
    
    //Listado de los puntos de calificación para cada bloque triturado:
    if(Score.LabelTimer > 0)
    {
        GUI.Label(Score.LabelRect, "+" + Score.JustAdded, Style);
    }
  

    //Etiquetas GUI y botones que controlarán el juego (play, stop, menú principal).
    //En primer lugar, vamos a ver la cantidad de espacio libre a las etiquetas de la GUI y cambiar los colores:
    var SpaceLeft = ScreenWidth - ((blockSize.x * blocksPerRow) + blockSize.x * 2);
    var StartRect = Rect((blockSize.x * blocksPerRow) + blockSize.x * 2, blockSize.y * 2, SpaceLeft, blockSize.y * 2);
    //var StopRect = Rect((blockSize.x * blocksPerRow) + blockSize.x * 2, blockSize.y * 2, SpaceLeft, blockSize.y * 2);
    //var MenuRect = Rect(StartRect.x, Screen.height - (blockSize.y * 2), SpaceLeft, blockSize.y);
    
    //Mostrando la puntuación más alta y la puntuación actual del reproductor:
    //Style.fontSize = blockSize.x;
    Style.fontSize = blockSize.x/1.8;//Barra lateral de estado
    if(IsRunning == true)
    {
        //GUI.Label(Rect(StartRect.x, StartRect.y + blockSize.y * 4.5, SpaceLeft, blockSize.y), "Score: " + Score.Value, Style);
    	GUI.Label(Rect(xP,yP-(yE*11),yE,yE), "Score: " + Score.Value, Style);
    }    
    if(Score.PlayForScore == true) //Si estamos jugando para llegar a una cantidad específica de puntuación:
    {
        //GUI.Label(Rect(StartRect.x, StartRect.y + blockSize.y * 2, SpaceLeft, blockSize.y), "Goal: " + Score.WinningScore, Style); 
        GUI.Label(Rect(xP,yP-(yE*16),yE,yE), "Win: " + Score.WinningScore, Style);  
    }
    if(Score.PlayForTime == true)
        {
            //GUI.Label(Rect(StartRect.x, StartRect.y + blockSize.y * 3, SpaceLeft, blockSize.y), "In " + Score.Minutes + " minutes", Style);
        	GUI.Label(Rect(xP,yP-(yE*16),yE,yE), "Win " + Score.Minutes + " minutes", Style);
        }
    GUI.Label(Rect(xP,yP-(yE*2),yE,yE), "Min Block: " + Score.Minblocks, Style); 
    //GUI.Label(Rect(StartRect.x, StartRect.y + blockSize.y * 7, SpaceLeft, blockSize.y), "High Score: " + Score.HighScore, Style);
    //GUI.Label(Rect(xP,yP,yE,yE), "Record: " + Score.HighScore, Style);
    //GUI.Label(Rect(StartRect.x, StartRect.y + blockSize.y * 6, SpaceLeft, blockSize.y), "Time: " + Minutes.ToString() + ":" + Seconds.ToString(), Style);
    GUI.Label(Rect(xP,yP-(yE*6),yE,yE), "Time: " + Minutes.ToString() + ":" + Seconds.ToString(), Style);
    
    //Special Powers GUI:
    var DynamicHeight : int = 9;
//------------------------------------------------------------------------------//    
   /* if(SpecialPowerEnabled == true)
    {
        for(var h : int = 0; h < SpecialPower.Length; h++) //Iniciar un bucle en la matriz poderes especiales.
        {
            GUI.Label(Rect(StartRect.x, StartRect.y + blockSize.y * DynamicHeight, SpaceLeft - blockSize.x, blockSize.y), SpecialPower[h].texture, Style);
            GUI.Label(Rect(StartRect.x, StartRect.y + blockSize.y * DynamicHeight, SpaceLeft + blockSize.x, blockSize.y), ": " + SpecialPower[h].Amount.ToString(), Style);
            DynamicHeight += 1.5;
        }
    }*/    
//-------------------------------------------------------------------------------//    
    
    //Cambiar el tamaño de la fuente:
    //Style.fontSize= blockSize.x*2;
    Style.fontSize = blockSize.x/4;
    
    
    //If the game hasn't started yet and the score is 0 (two conditions needed to confirm that the game isn't running).
    /*if(IsRunning == false && Score.Value == 0 && GameOverTimer == 0)
    {
        Style.normal.textColor = Color.white;
        if(StartRect.Contains(Event.current.mousePosition)) //Check if the player's mouse is over the start label:
        {
            Style.normal.textColor = Purple;
            if(Input.GetMouseButton(0) && CountDown == 0) //If the player clicks on the label, start the game!
            {
                AreblocksReady = false;
                CountDown = 3.5; //Start the count down timer.
                if(CountDownSound) PlaySound(CountDownSound);
            }
        }
        GUI.Label(StartRect, "Start", Style);
    }   
    
    else if(IsRunning == true && AreblocksReady == true) //If the game is running:
    {
        Style.normal.textColor = Color.white;
        if(StopRect.Contains(Event.current.mousePosition)) //Check if the player's mouse is over the stop game label:
        {
            Style.normal.textColor = Purple;
            if(Input.GetMouseButton(0)) //If the player clicks on the label, stop the game!
            {
                StopGame();
            }
        }
        GUI.Label(StopRect, "Stop", Style);
    }
    
    Style.normal.textColor = Color.white;
    if(MenuRect.Contains(Event.current.mousePosition)) //Check if the player's mouse is over the main menu label:
    {
        Style.normal.textColor = Purple;
        if(Input.GetMouseButton(0)) //If the player clicks on the label, stop the game and load the main menu scene:
        {
            StopGame();
            Application.LoadLevel("Main Menu"); //Insert here the name/number of your main menu ID.
        }
    }
    GUI.Label(MenuRect, "Main Menu", Style);*/
}


function Createblocks ()
{
    
    var SlotFound : boolean = false;
    for(var i : int = 0; i < Slot.Length; i++) //En primer lugar, iniciar un bucle en las ranuras actuales:
    {
        if(i == (blocksPerRow * Rows) - (blocksPerRow * UsedRows)) //Si esta es la primera fila:
        {
            SlotFound = true;
        }
        if(SlotFound == true)
        {   
            //Set the slot's new info:
            Slot[i].IsTaken = true;
            var Randomblock : int = Random.Range(0,cantidadBloques); //Este elegirá al azar uno de los bloques:
            Slot[i].Name = block[Randomblock].Name;
            Slot[i].texture = block[Randomblock].texture;
            Slot[i].Moving = true;
            Slot[i].MovingRect = Slot[i].rect;
            Slot[i].SpecialPowerName = "None";
        }
        if(SlotFound == false)
        {
            //Restablecer las otras ranuras por si acaso:
            Slot[i].IsTaken = false;
            Slot[i].Name = "None";
            Slot[i].Moving = false;
        }
    }
    
    AreblocksReady = true;
}

function SearchForblocks()
{
    SearchOver = true;
    var Match : boolean = false;
    
    for(var d : int = 0; d < Elimination.Length; d++) //Iniciar un bucle en el contenido de matriz eliminación:
    {
        for(var c : int = 0; c < Slot.Length; c++) //Iniciar un nuevo bucle en las ranuras actuales:
        {
            /*Estaremos buscando por todas las ranuras de encontrar los bloques que coinciden con el bloque que se hace clic,
             y comprobar si estos bloques en contacto con él directamente o no directamente.*/
            if(Slot[c].IsTaken == true && Slot[c].Name == Slot[Elimination[d]].Name && c != Elimination[d] && Slot[c].Moving == false) //Compruebe si se toma la ranura tiene el mismo nombre.
            {
                //Compruebe si la ranura de corriente está en contacto con una de las ranuras para eliminar, en cualquier dirección:
                if(Slot[c].Bottom == Elimination[d] || Slot[c].Top == Elimination[d] || Slot[c].Left == Elimination[d] || Slot[c].Right == Elimination[d])
                {
                    Match = false;
                    for(var e : int = 0; e < Elimination.Length; e++) //Iniciar este bucle de nuevo para comprobar que no estamos agregando los mismos bloques.
                    {
                        if(c == Elimination[e]) //Hay un partido
                        {
                            Match = true;
                        }
                    }
                    if(Match == false)
                    {
                        //Agregar el bloque actual a la matriz de la eliminación.
                        var TempArray = new Array(Elimination);
	                    TempArray.Add(c);
	                    Elimination = TempArray.ToBuiltin(int);
	                    SearchOver = false;
	                }    
                }
            }
        }                          
    }
    if(SearchOver == false)
    {
        SearchForblocks();
    }
}

function AddRow()
{
    //Primero tendremos que mover los bloques de una fila más arriba para que podamos tener espacio para insertar una nueva fila.
    var SlotFound : boolean = false;
    var PlayAlert : boolean = false;
    
    
    for(var i : int = 0; i < Slot.Length; i++) //Iniciar un bucle en las ranuras actuales:
    {
        if(i <= blocksPerRow - 1 && Slot[i].IsTaken == true) //Compruebe si la ranura actual no está en la última fila.
        {
            StopGame();
            return;
        }
        if(Slot[i].IsTaken == true && SlotFound == false) //Si esta es la primera fila:
        {
            SlotFound = true; //Ranura Primera tomado encontrado.
            //Compruebe si llegamos a las tres últimas filas:
            if(i >= 0 && i <= blocksPerRow * 3)
            {
                PlayAlert = true; //Hacer esto cierto para reproducir el sonido de pitido / alerta / alarma después.
            }
        }
        if(SlotFound == true && Slot[i].IsTaken == true)
        {   
            //Establecer las ranuras nueva información (la ranura que está en la parte superior de éste).
            Slot[i - blocksPerRow].IsTaken = true;
            Slot[i - blocksPerRow].Name = Slot[i].Name;
            Slot[i - blocksPerRow].texture = Slot[i].texture;
            Slot[i - blocksPerRow].Moving = true;
            Slot[i - blocksPerRow].MovingRect = Slot[i].rect;
            Slot[i - blocksPerRow].MovingAmount = -blockSize.y;
            Slot[i - blocksPerRow].SpecialPowerName = Slot[i].SpecialPowerName;
            //Desactive la casilla actual info:
            Slot[i].IsTaken = false;
            Slot[i].Name = "None";
            Slot[i].texture = null;
            Slot[i].Moving = false;
            Slot[i].SpecialPowerName = "None";
        }
    }
    /*Ahora que nos movemos los bloques de una fila más arriba, vamos a añadir la nueva fila. Vamos a buscar 
    para el primer bloque de la primera fila:*/


    var Found : boolean = false;
    for(var s : int = 0; s < Slot.Length; s++) //Nuevo look:
    {
        if(s == (blocksPerRow * Rows) - (blocksPerRow)) //Si este es el primer bloque de la primera fila:
        {
            Found = true;
        }
        if(Found == true)
        {
            //Set the slot's new info:
            Slot[s].IsTaken = true;
            var Randomblock : int = Random.Range(0,cantidadBloques); //Este elegirá al azar uno de los bloques:
            Slot[s].Name = block[Randomblock].Name;
            Slot[s].texture = block[Randomblock].texture;
            Slot[s].Moving = true;
            Slot[s].MovingRect = Rect(Slot[s].rect.x, Slot[s].rect.y + blockSize.y, blockSize.x, blockSize.y); 
            Slot[s].MovingAmount = -blockSize.y;
            if(SpecialPowerEnabled == true && AddedRows >= RowsToAddPowers)
            {
                var RandomSpecialPower : int = Random.Range(0, SpecialPowerRange);
                for(var d : int = 0; d < SpecialPower.Length; d++) //Nueva imagen
                {
                    if(RandomSpecialPower == d)
                    {
                        Slot[s].SpecialPowerName = SpecialPower[d].Name;
                    }
                } 
            }     
        }
    }
    
    if(NewRowSound) //Juega el nuevo sonido fila.
    {
        PlaySound(NewRowSound);
    }
    if(PlayAlert) //Si llegamos a las tres últimas filas.
    {
        if(AlertSound) PlaySound(AlertSound);
    }
}

function StopGame()
{
//---------------------------------------------------------------------------------    
    IsRunning = false; //Detener el juego.
   // ShowCrushs = false;
    
    //Reajuste el tiempo
    Seconds = 0;
    Minutes = 0;
    
    //en primer lugar, restablecer la información de la velocidad:
    Speed.CurrentValue = Speed.Default;
    Speed.CountDown += Speed.CurrentValue;
    
    //Reiniciar la información filas:
    AddedRows = 0;
    RowsToChangeSpeed = 0;
    
    //GameOverMsg = "Score: " + Score.Value;
    GameOverTimer = 3; //Comience la cuenta atrás que se oculta el msg.
//----------------------------------------------------------------------------------
    if(NewRecordSound) PlaySound(NewRecordSound);
    if(Score.PlayForTime == true)
    	{
    	if(checkWin==true) //Si el tiempo ha terminado.
            {
            if(WinningSound) PlaySound(WinningSound);
            GameOverMsg = "Winner";
            print("gane");
           	//if(Input.touchCount>0 || Input.GetMouseButton(0))
			      		//{
			      		print("gane2");
			       		yield WaitForSeconds(2);
			       		Application.LoadLevel("Menu");
			       		//}
            }
         else	
         	{
         	GameOverMsg = "Game Over";
	        //GameOverTimer = 3; //Comience la cuenta atrás que se oculta el msg.
	        Musica.sonido=3;
	        if(GameOverSound) PlaySound(GameOverSound);
	        checkGameOver=true;
         	}
    	}
    
    if(Score.PlayForScore == true) //Si estamos jugando por unos puntos de calificación específicos
    {
        if(Score.Value >= Score.WinningScore) //Si llegamos a la cantidad de puntos de calificación para ganar:
        {
            if(WinningSound) PlaySound(WinningSound);
            GameOverMsg = "Winner";
			if(Input.touchCount>0 || Input.GetMouseButton(0))
			      		{
			       		yield WaitForSeconds(2);
			       		Application.LoadLevel("Menu");
			       		}
        }
        else
        {
	        GameOverMsg = "Game Over";
	        Musica.sonido=3;
	        //GameOverTimer = 3; //Comience la cuenta atrás que se oculta el msg.
	        if(GameOverSound) PlaySound(GameOverSound);
	        checkGameOver=true;
        }
        //GameOverTimer = 3; //Comience la cuenta atrás que se oculta el msg.
    }
    
    //New record:
    else if(Score.Value > Score.HighScore) //Si la puntuación es superior a la puntuación más alta.
    {
        //New record!
        Score.HighScore = Score.Value;
        PlayerPrefs.SetInt("HighScore"+LevelName, Score.HighScore); //Save the new record.
        
        //GameOverMsg = "Record: " + Score.HighScore;
        //GameOverTimer = 3; //Comience la cuenta atrás que se oculta el msg.
        if(Input.touchCount>0 || Input.GetMouseButton(0))
			      		{
			       		yield WaitForSeconds(2);
			       		Application.LoadLevel("Menu");
			       		}
        if(NewRecordSound) PlaySound(NewRecordSound);
    }
//--------------------------------------------------------------------------------------    
    //Jugador no ha ganado o no hizo una nueva puntuación
    /*else
    {
        GameOverMsg = "Game Over";
        //GameOverTimer = 3; //Comience la cuenta atrás que se oculta el msg.
        if(GameOverSound) PlaySound(GameOverSound);
        	checkGameOver=true;
    }*/
//----------------------------------------------------------------------------------------    
    //Reset the special powers amount:
    for(var i : int = 0; i < SpecialPower.Length; i++) //Iniciar un bucle en la matriz poderes especiales.
    {
        SpecialPower[i].Amount = 0;
    }
    
    
    if(Score.PlayForTime==true)
    	{
    	Puntuacion.puntajeTime=Score.Value;
    	Modalidad=false;
    	}
    if(Score.PlayForScore==true)
    	{
    	Puntuacion.puntajeScore=Score.Value;
    	Modalidad=true;
    	}
    Score.Value = 1;
    //Reiniciar la partitura:
    yield WaitForSeconds(1); //Wait for 1 second...
    Score.Value = 0;
}


function PlaySound (Sound : AudioClip)
{
    if(Sound == Music)
    {
        if(IsMusicON == true)
        {
            GetComponent.<AudioSource>().PlayOneShot(Sound);
        }
    }
    else
    {
        if(IsSoundON == true)
        {
            GetComponent.<AudioSource>().PlayOneShot(Sound);
        }
    }
}